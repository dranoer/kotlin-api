package com.isatelco.nasa

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("apod")
    fun getPhoto(@Query("api_key") api_key : String = "Hnq4PVYy5cWYogHCxRIE8K62ZiylJFKjdgmtGTIA") : Observable<Photo>

}