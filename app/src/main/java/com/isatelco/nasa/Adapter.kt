package com.isatelco.nasa

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide

class Adapter (val context: Context)
    : RecyclerView.Adapter<Adapter.ViewHolder>() {

        private val photos: MutableList<Photo> = mutableListOf()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false))
        }

        override fun getItemCount(): Int {
            return photos.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindModel(photos[position])
        }

        fun setPhoto(data: Photo) {
            photos.add(data)
            notifyDataSetChanged()
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            val Image : ImageView = itemView.findViewById(R.id.imageview)

            fun bindModel(photo: Photo) {
                Glide.with(context)
                        .load(photo.url)
                        .into(Image)
            }
        }
}